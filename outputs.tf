output "accept_status" {
  description = "*string* Accepted status of the peer VPC Connection"
  value       = element(concat(aws_vpc_peering_connection.peer.*.accept_status, []), 0)
}

output "peer_route_tables" {
  description = "*string* Route tables of the private peering"
  value       = element(concat(aws_route.private_peering.*.id, []), 0)
}

output "requester_id" {
  description = "**string* ID of the peer VPC Connection"
  value       = element(concat(aws_vpc_peering_connection.peer.*.id, []), 0)
}
