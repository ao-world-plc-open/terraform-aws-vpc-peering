# Terraform Module: AWS VPC Peering

This module is for creating the peering object and modifying routing tables
to allow networking traffic between VPC's in different accounts.

* [Example Usage](#example-usage)
  * [Basic](#basic)
  * [Advanced](#advanced)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

This will setup a peering request from a VPC in your account, to a VPC in
another account with the first module. The accepting account can use the
second module to accept the peer and configure routing correctly on their
end.

```hcl
module "requesting_account" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-vpc.git?ref=v1.1.0"

  envtype         = "QA"
  peer_account_id = "accepting-account-id"
  # These should be the subnet CIDRs the accepting account is coming from so
  # traffic can be routed to it correctly
  peer_cidr_blocks = [
    "192.168.0.0/24",
    "192.168.1.0/24",
    "192.168.2.0/24"
  ]
  peer_vpc_id = "accepting-account-vpc-id"
  vpc_id      = "requesting_account-vpc-id"
  vpc_private_route_tables = [
    "requesting-route-table-id1",
    "requesting-route-table-id2",
    "requesting-route-table-id3"
  ]
}

module "accepting_account" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-vpc.git?ref=v1.1.0"

  envtype                = "QA"
  multi_account_acceptor = 1
  peer_account_id        = "requesting-account-id"
  # These should be the subnet CIDRs the requesting account is coming from so
  # traffic can be routed back corretcly
  peer_cidr_blocks = [
    "172.16.0.0/24",
    "172.16.1.0/24",
    "172.16.2.0/24"
  ]
  peer_vpc_id = "requesting-account-vpc-id"
  vpc_id      = "accepting-account-vpc-id"
  vpc_private_route_tables = [
    "accepting-route-table-id1",
    "accepting-route-table-id2",
    "accepting-route-table-id3"
  ]
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| envtype | The type of environment the peering is being deployed to i.e. QA | `string` | n/a | yes |
| peer\_account\_id | The AWS Account number of the account to peer to | `string` | n/a | yes |
| peer\_cidr\_blocks | n/a | `list(string)` | n/a | yes |
| peer\_vpc\_id | The VPC ID of the VPC to peer to | `string` | n/a | yes |
| vpc\_id | The VPC ID of the VPC to peer from | `string` | n/a | yes |
| vpc\_private\_route\_tables | A list of route tables that require a route adding for the peer CIDR blocks | `list(string)` | n/a | yes |
| auto\_accept\_peer | If this terraform configuration is used across a single account this can be set to true to enable the peering connections to work  from the requester | `string` | `"false"` | no |
| custom\_tags | A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence | `map(string)` | `{}` | no |
| enabled | Detrmines if the peering should be applied | `string` | `1` | no |
| multi\_account\_acceptor | If this terraform configuration is used across multiple accounts this can be set to true to find the peering connection | `string` | `0` | no |
| peer\_vpc\_region | The region which the accepter VPC is in | `string` | `"eu-west-1"` | no |

## Outputs

| Name | Description |
|------|-------------|
| accept\_status | *string* Accepted status of the peer VPC Connection |
| peer\_route\_tables | *string* Route tables of the private peering |
| requester\_id | **string* ID of the peer VPC Connection |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.