# Change Log

## v1.1.0 (21/12/2021)

* Added .terraform-doc.yml to auto generate (mostly) README.md
* Added basic example config for README.md
* All variables and outputs have descriptions and types, and have been
  organised alphabetically
* Ran `terraform fmt` on the repository
* Updated CONTRIBUTING.md to cover running fmt and ensuring docs are
  regenerated
* Added .gitlab-ci.yml to do basic checks for fmt and out of date docs

## v1.0.0 (01/06/2021)

* Added minimum provider version constraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled
* Terraform 0.13 now the minimum version due to use of required_providers syntax

## v0.2.0 (29/05/2020)

### Variables
* Added new variable `auto_accept_peer` to enable single account peering

## v0.1.0 (02/09/2019)

### Features

* Initial Commit

### Fixes

*N/A*