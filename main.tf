resource "aws_vpc_peering_connection" "peer" {
  count = var.multi_account_acceptor == 1 ? 0 : 1 * var.enabled

  auto_accept   = var.auto_accept_peer
  peer_owner_id = var.peer_account_id
  peer_region   = var.peer_vpc_region
  peer_vpc_id   = var.peer_vpc_id
  tags = merge({
    "Name"    = "VPC peering between ${var.vpc_id} and ${var.peer_vpc_id}",
    "EnvType" = var.envtype
  }, var.custom_tags)
  vpc_id = var.vpc_id

  requester {
    allow_remote_vpc_dns_resolution = true
  }
}

data "aws_vpc_peering_connection" "peer_connection" {
  count = var.multi_account_acceptor == 1 ? 1 * var.enabled : 0

  owner_id    = var.peer_account_id
  peer_vpc_id = var.vpc_id
  vpc_id      = var.peer_vpc_id

  filter {
    name   = "status-code"
    values = ["active", "pending-acceptance"]
  }
}

resource "aws_vpc_peering_connection_accepter" "peer_accepter" {
  count = var.multi_account_acceptor == 1 ? 1 * var.enabled : 0

  auto_accept = true
  tags = merge({
    "Name"    = "VPC peering between ${var.vpc_id} and ${var.peer_vpc_id}",
    "EnvType" = var.envtype
  }, var.custom_tags)
  vpc_peering_connection_id = data.aws_vpc_peering_connection.peer_connection[0].id
}

resource "aws_route" "private_peering" {
  count = length(var.peer_cidr_blocks) * length(var.vpc_private_route_tables) * var.enabled

  destination_cidr_block    = var.peer_cidr_blocks[count.index % length(var.peer_cidr_blocks)]
  route_table_id            = var.vpc_private_route_tables[count.index / length(var.peer_cidr_blocks)]
  vpc_peering_connection_id = var.multi_account_acceptor == 1 ? join(" ", data.aws_vpc_peering_connection.peer_connection.*.id) : join(" ", aws_vpc_peering_connection.peer.*.id)
}
