module "requesting_account" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-vpc.git?ref=v1.1.0"

  envtype         = "QA"
  peer_account_id = "accepting-account-id"
  # These should be the subnet CIDRs the accepting account is coming from so
  # traffic can be routed to it correctly
  peer_cidr_blocks = [
    "192.168.0.0/24",
    "192.168.1.0/24",
    "192.168.2.0/24"
  ]
  peer_vpc_id = "accepting-account-vpc-id"
  vpc_id      = "requesting_account-vpc-id"
  vpc_private_route_tables = [
    "requesting-route-table-id1",
    "requesting-route-table-id2",
    "requesting-route-table-id3"
  ]
}

module "accepting_account" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-vpc.git?ref=v1.1.0"

  envtype                = "QA"
  multi_account_acceptor = 1
  peer_account_id        = "requesting-account-id"
  # These should be the subnet CIDRs the requesting account is coming from so
  # traffic can be routed back corretcly
  peer_cidr_blocks = [
    "172.16.0.0/24",
    "172.16.1.0/24",
    "172.16.2.0/24"
  ]
  peer_vpc_id = "requesting-account-vpc-id"
  vpc_id      = "accepting-account-vpc-id"
  vpc_private_route_tables = [
    "accepting-route-table-id1",
    "accepting-route-table-id2",
    "accepting-route-table-id3"
  ]
}