variable "auto_accept_peer" {
  description = "If this terraform configuration is used across a single account this can be set to true to enable the peering connections to work  from the requester"
  type        = string
  default     = "false"
}

variable "custom_tags" {
  description = "A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(string)
  default     = {}
}

variable "enabled" {
  description = "Detrmines if the peering should be applied"
  type        = string
  default     = 1
}

variable "envtype" {
  description = "The type of environment the peering is being deployed to i.e. QA"
  type        = string
}

variable "multi_account_acceptor" {
  type        = string
  description = "If this terraform configuration is used across multiple accounts this can be set to true to find the peering connection"
  default     = 0
}

variable "peer_cidr_blocks" {
  type = list(string)
}

variable "peer_account_id" {
  description = "The AWS Account number of the account to peer to"
  type        = string
}

variable "peer_vpc_id" {
  description = "The VPC ID of the VPC to peer to"
  type        = string
}

variable "peer_vpc_region" {
  description = "The region which the accepter VPC is in"
  type        = string
  default     = "eu-west-1"
}

variable "vpc_id" {
  description = "The VPC ID of the VPC to peer from"
  type        = string
}

variable "vpc_private_route_tables" {
  description = "A list of route tables that require a route adding for the peer CIDR blocks"
  type        = list(string)
}
